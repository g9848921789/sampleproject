Feature: API Execution test

  Scenario: Valid API endpoint returns 200 response
    Given a valid url 'https://searchconsole.googleapis.com/$discovery/rest?version=v1'
    When hit the valid url
    Then api should return 200 response code


  Scenario: Invalid API endpoint returns 404 response
    Given a invalid url 'https://searchconsole.googleapis.com/$discovery/rest?version=v9'
    When hit the invalid url
    Then api should return 404 response code