package nl.serenitytest.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import nl.serenitytest.app.APIHandler;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpResponse;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ApiExecutionDefinition {

    private final APIHandler handler = new APIHandler();

    private String URI;
    private HttpResponse response;

    @Given("a valid url {string}")
    public void givenValidURL(String endpoint) {
        this.URI = endpoint;
    }

    @When("hit the valid url")
    public void hitTheCorrectURL() throws URISyntaxException, IOException, InterruptedException {
        response = handler.trigger(URI);
    }

    @Then("api should return 200 response code")
    public void apiShoudReturnOKResponse() {
        assertThat(200).isEqualTo(response.statusCode());
    }

    @Given("a invalid url {string}")
    public void givenInValidAURL(String endpoint) {
        this.URI = endpoint;
    }

    @When("hit the invalid url")
    public void hitTheIncorrectURL() throws URISyntaxException, IOException, InterruptedException {
        response = handler.trigger(URI);
    }

    @Then("api should return 404 response code")
    public void apiShoudReturnNotFoundResponse() {
        assertThat(404).isEqualTo(response.statusCode());
    }


}
